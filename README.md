# Cobuild Pluggable Module Boilerplate

This is a boilerplate to create pluggable and easly configurable npm modules 
with a common interface.

# Requiments

This project uses`cobuild-npm-module` so you have to define the *NODE_PATH* environment variable with the root project
path in every project where this module is used.

## Tests
To run the test you just need to run `npm test`

## Quality Code
This project use all the style rules described by [Standard.js](https://standardjs.com). To lint the code you can run `npm run lint`.
