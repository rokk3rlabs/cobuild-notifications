/**
 * Created by garusis on 15/05/18.
 */
const {CobuildCustom} = require('./lib/CobuildCustom')

/**
 *
 * @param {Object} options Settings to configure the module instance
 * @param {Boolean} options.module_enabled The status (enable/disable) of this module
 * @param {String} [options.data.instance_id=nextIndex] The id for this module instance. If not provided a serial index will be assigned.
 * @returns {CobuildPluggable}
 * @throws {JoiError} A Joi error is thrown when the options aren't valid agains the objectSchema.
 */
module.exports = function (options) {
  return new CobuildCustom(options)
}
