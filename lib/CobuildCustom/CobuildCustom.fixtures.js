/**
 * Created by garusis on 22/06/18.
 */
const fixtures = {}

fixtures.validOptions = {
  module_enabled: true,
  type: 'phone',
  apiKey: process.env.API_KEY,
  apiSecret: process.env.API_SECRET,
  region: process.env.REGION,
  service: process.env.SERVICE
}

fixtures.invalidOptions = {
  // module_enabled is required
}

module.exports = fixtures
