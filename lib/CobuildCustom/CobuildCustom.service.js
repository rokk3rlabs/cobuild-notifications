
const AWS           = require('aws-sdk');
const Twilio        = require('twilio')

var smsProviders = {
  'TWILIO': smsTwilio,
  'AWS': smsAWS
};
  
module.exports = (provider, options) => {
  return {
    send: (smsData) => {
      return smsProviders[provider](smsData, options);
    }
  }
};

function smsTwilio(data, options) {
  const twilio = Twilio(options.key, options.secret);

  let params = {
    from: options.from,
    body: data.message,
    to: data.to
  }

  return new Promise((resolve, reject) => {
    twilio.messages
    .create(params)
    .then(res => {
      resolve(res)
    })
    .catch(reject);
  });
}

function smsAWS(data, options) {

  AWS.config = new AWS.Config();
  AWS.config.accessKeyId = options.key;
  AWS.config.secretAccessKey = options.secret;
  AWS.config.region = options.region;

  const sns = new AWS.SNS();

  let params = {
    Message: data.message, /* required */
    PhoneNumber: data.to
  };
  
  return new Promise((resolve, reject) => {
    sns.publish(params, (err, data) => {
      if (err)
        return reject(err);
      resolve(data);
    });
  })
  
}