/* eslint-disable no-undef,no-unused-expressions */
/**
 * Created by garusis on 22/06/18.
 */
const {CobuildCustom} = require('./index')
const {validOptions, invalidOptions} = require('./CobuildCustom.fixtures')

describe('CobuildPluggable', function () {
  describe('#Constructor', function () {
    it('Should create a new CobuildCustom instance with valid options', function () {
      const instance = new CobuildCustom(validOptions)
      expect(instance instanceof CobuildCustom).to.be.true
    })

    it('Should send a SMS', function (done) {
      const instance = new CobuildCustom(validOptions)
      instance
        .renderAndSendMessage('SMS test', {
          to: process.env.TO,
          vars: [],
          templateVars: {
            token: Math.random().toString(36).substr(2, 6)
          }
        })
        .then(res => {
          expect(instance instanceof CobuildCustom).to.be.true
          done()
        })
        .catch(err => {
          console.error('Error on send token notification', err)
          done(err)
        })
      
    })


    it('Shouldn\'t create a CobuildCustom instance with invalid options', function () {
      try {
        const instance = new CobuildCustom(invalidOptions)
        expect(instance instanceof CobuildCustom).to.be.false
      } catch (err) {
        err.should.be.an('Error')
        err.name.should.equal('ValidationError')
      }
    })
  })
})
