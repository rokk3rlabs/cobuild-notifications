/**
 * Created by garusis on 22/06/18.
 */
const _                   = require('lodash')
const fs                  = require('fs')
const Joi                 = require('joi')
const {CobuildPluggable}  = require('../CobuildPluggable')
const packageJson         = require('../../package.json')
const handlebars          = require('handlebars')
const deepmerge           = require('deepmerge')
const MandrilHook         = require('cobuild-mandrill-hook')
const i18n                = require('i18n')
const Twilio              = require('twilio')

const moduleData = {
  module_name: packageJson.name,
  module_label: packageJson.name,
  module_description: packageJson.description
}

i18n.configure({
  locales: ['en', 'es'],
  directory: process.cwd() + '/locales',
  defaultLocale: 'en',
  objectNotation: true
});

const createSchema = Joi.object().keys({
  from: Joi.string().label('From Email or Phone')
    .description('The sender\'s email or phone'),
  region: Joi.string().label('Region')
    .description('Region'),
  fromName: Joi.string().label('From Name')
    .description('The sender\'s name'),
  apiKey: Joi.string().required().label('Mandrill, Twilio or SNS API Key')
    .description('Mandrill or Twilio API Key'),
  apiSecret: Joi.string().label('Twilio Account or SNS Secret')
    .description('Twilio Account or SNS Secret'),
  type: Joi.string().default('email').label('Notification type')
    .description('Notification Type'),
  service:  Joi.string().default('mandrill').label('Notification Service')
  .description('twilio or aws').valid('twilio', 'aws')
})
const configSchema = createSchema

class CobuildCustom extends CobuildPluggable {
  /**
   *
   * @param {Object} options Settings to configure the module instance
   * @param {String} options.module_enabled The status (enable/disable) of this module
   * @param {String} [options.data.instance_id=nextIndex] The id for this module instance. If not provided a serial index will be assigned.
   * @returns {CobuildCustom}
   * @throws {JoiError} A Joi error is thrown when the options aren't valid agains the objectSchema.
   */
  constructor (options) {
    const moduleOptions = _.cloneDeep(moduleData)
    moduleOptions.data = _.cloneDeep(options)
    super(moduleOptions, createSchema, configSchema)
  }

  set options (options) {
    this._options = deepmerge(this.options, options)
    this._type = this._options.data.type
    console.log('this ', this._options)
    if (this._type === 'phone') {
      let options = {
        secret: this._options.data.apiSecret,
        key: this._options.data.apiKey,
        from: this._options.data.from,
        region: this._options.data.region
      }
      this._smsService = require('./CobuildCustom.service')(this._options.data.service.toUpperCase(), options);
    }
    else {
      let options = _.pick(this._options.data, ['fromName', 'apiKey'])
      options.fromEmail = this._options.data.from
      this.mailer = MandrilHook(options)
    }
    this.emit('config', this.options)
  }

  get options () {
    return this._options || {}
  }

  sendMessage (body, optionsMessage) {
    return new Promise((resolve, reject) => {

      this._smsService.send({
        message: i18n.__(body, optionsMessage.templateVars.token),
        to: optionsMessage.to
      })
      .then(message => {
        resolve(message)
      })
      .catch(err => {
        reject(err)
      });
    })
  }

  sendEmail (optionsMessage) {
    return new Promise((resolve, reject) => {
      return this.mailer.send(optionsMessage, (error, result) => error ? reject(error) : resolve(result))
    })
  }


  renderAndSendMessage (body, optionsMessage) {

    i18n.setLocale(optionsMessage.lang || 'en');
    
    if (!optionsMessage.to) {
      return Promise.reject('to is required')
    }

    if (this._options.data.type === 'phone')
      return this.sendMessage(body, optionsMessage)

    return new Promise((resolve, reject) =>
      fs.readFile(`${process.cwd()}/${body}`, 'utf-8', (error, content) => error ? reject(error) : resolve(content)))
      .then((content) => {
        optionsMessage.html = handlebars.compile(content)(optionsMessage.templateVars)
        return this.sendEmail(optionsMessage)
      })
  }
}

module.exports = {CobuildCustom}
