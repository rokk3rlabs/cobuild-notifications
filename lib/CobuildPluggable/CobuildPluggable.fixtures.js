/**
 * Created by garusis on 22/06/18.
 */
const Joi = require('joi')
const {updateSchema} = require('./main_schemas')

const fixtures = {}

fixtures.objectSchema = Joi.object().keys({
  requiredKey: Joi.string().required()
})

fixtures.validOptions = {
  module_name: 'my_custom_module',
  module_label: 'My Custom Module',
  module_description: 'This is a custom Module',
  data: {
    module_enabled: true,
    requiredKey: 'test key'
  }
}

fixtures.validOptionsWithCustomInstanceId = {
  module_name: 'my_custom_module',
  module_label: 'My Custom Module',
  module_description: 'This is a custom Module',
  data: {
    module_enabled: true,
    instance_id: 'my_instance',
    requiredKey: 'test key'
  }
}

fixtures.otherValidOptions = {
  data: {
    module_enabled: false,
    requiredKey: 'another key'
  }
}

fixtures.otherValidOptionsWithCustomInstanceId = {
  module_name: fixtures.validOptionsWithCustomInstanceId.module_name,
  data: {
    module_enabled: false,
    instance_id: fixtures.validOptionsWithCustomInstanceId.data.instance_id,
    requiredKey: 'changed key'
  }
}

fixtures.otherInstanceValidOptionsWithCustomInstanceId = {
  module_name: fixtures.validOptionsWithCustomInstanceId.module_name,
  data: {
    module_enabled: false,
    instance_id: 'ot_instance',
    requiredKey: 'changed key'
  }
}

fixtures.otherModuleValidOptionsWithCustomInstanceId = {
  module_name: 'another_module',
  data: {
    module_enabled: false,
    instance_id: fixtures.validOptionsWithCustomInstanceId.data.instance_id,
    requiredKey: 'changed key'
  }
}

fixtures.otherInvalidOptionsWithCustomInstanceId = {
  module_name: fixtures.validOptionsWithCustomInstanceId.module_name,
  data: {
    instance_id: fixtures.validOptionsWithCustomInstanceId.data.instance_id
  }
}

fixtures.invalidOptions = {
  module_name: 'my_custom_module',
  module_label: 'My Custom Module',
  module_description: 'This is a custom Module',
  data: {
    // module_enabled is required
  }
}

fixtures.validDescriptionWithCustomInstanceId = {
  data: fixtures.validOptionsWithCustomInstanceId,
  schema: updateSchema.concat(Joi.object().keys({
    data: fixtures.objectSchema
  })).describe()
}

module.exports = fixtures
