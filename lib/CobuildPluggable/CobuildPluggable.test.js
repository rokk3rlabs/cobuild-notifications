/* eslint-disable no-undef,no-unused-expressions */
/**
 * Created by garusis on 22/06/18.
 */
const _ = require('lodash')
const sinon = require('sinon')
const {CobuildPluggable} = require('./index')
const {
  objectSchema, validOptions, validOptionsWithCustomInstanceId, otherValidOptions,
  invalidOptions, validDescriptionWithCustomInstanceId, otherInstanceValidOptionsWithCustomInstanceId,
  otherModuleValidOptionsWithCustomInstanceId, otherValidOptionsWithCustomInstanceId,
  otherInvalidOptionsWithCustomInstanceId
} = require('./CobuildPluggable.fixtures')

describe('CobuildPluggable', function () {
  describe('#Constructor', function () {
    it('Should create a new CobuildPluggable instance with a valid ObjectSchema', function () {
      const instance = new CobuildPluggable(validOptions, objectSchema)
      expect(instance instanceof CobuildPluggable).to.be.true
    })

    it('Should create a new CobuildPluggable instance with a custom instance_id', function () {
      const instance = new CobuildPluggable(validOptionsWithCustomInstanceId, objectSchema)
      expect(instance instanceof CobuildPluggable).to.be.true
      instance.options.data.instance_id.should.to.be.equal(validOptionsWithCustomInstanceId.data.instance_id)
    })

    it('Shouldn\'t create a CobuildPluggable instance with an invalid ObjectSchema', function () {
      try {
        const instance = new CobuildPluggable(invalidOptions, objectSchema)
        expect(instance instanceof CobuildPluggable).to.be.false
      } catch (err) {
        err.should.be.an('Error')
        err.name.should.equal('ValidationError')
      }
    })
  })

  describe('#config', function () {
    it('Should allow config the instance with a valid ObjectSchema', function () {
      const instance = new CobuildPluggable(validOptions, objectSchema)
      expect(instance instanceof CobuildPluggable).to.be.true
      instance.config(otherValidOptions)
    })

    it('Shouldn\'t config the instance with an invalid ObjectSchema', function () {
      try {
        const instance = new CobuildPluggable(invalidOptions, objectSchema)
        expect(instance instanceof CobuildPluggable).to.be.true
        instance.config(invalidOptions)
      } catch (err) {
        err.should.be.an('Error')
        err.name.should.equal('ValidationError')
      }
    })
  })

  describe('#describe', function () {
    it('Should get the current settings and the objectSchema description', function () {
      const instance = new CobuildPluggable(validOptionsWithCustomInstanceId, objectSchema)
      expect(instance instanceof CobuildPluggable).to.be.true
      instance.describe().should.to.be.deep.equal(validDescriptionWithCustomInstanceId)
    })
  })

  describe('events', function () {
    describe('#config', function () {
      it('Should be fired when instance.config is called', function () {
        const instance = new CobuildPluggable(validOptions, objectSchema)
        expect(instance instanceof CobuildPluggable).to.be.true

        const listener = sinon.spy()
        instance.on('config', listener)

        instance.config(otherValidOptions)
        listener.calledOnce.should.to.be.true

        const args = listener.getCall(0).args
        args.length.should.to.be.equal(1)
        args[0].should.to.be.an('object')
      })
    })
  })

  describe('#middlewares', function () {
    describe('#describe', function () {
      it('Should overwrite and restore the res.send function and call the original function with an array that includes the module description', function () {
        const instance = new CobuildPluggable(validOptions, objectSchema)
        expect(instance instanceof CobuildPluggable).to.be.true

        const originalSend = sinon.spy()
        const res = {
          send: originalSend
        }
        const body = []

        const next = sinon.spy(function (err) {
          expect(err).to.not.exist
          res.send.should.to.be.not.equal(originalSend)
          res.send(body)
        })

        instance.middlewares.describe({}, res, next)
        next.calledOnce.should.to.be.true
        res.send.should.to.be.equal(originalSend)
        originalSend.calledOnce.should.to.be.true
        originalSend.calledWith(body).should.to.be.true

        const description = body.find((description) => description.data.module_name === instance.options.module_name && description.data.data.instance_id === instance.options.data.instance_id)
        expect(description).to.exist
        description.should.to.be.deep.equal(instance.describe())
      })
    })

    describe('#config', function () {
      it('Should ignore changes when there aren\'t options for it', function () {
        const instance = new CobuildPluggable(validOptionsWithCustomInstanceId, objectSchema)
        expect(instance instanceof CobuildPluggable).to.be.true

        const req = {
          body: [
            otherModuleValidOptionsWithCustomInstanceId,
            otherInstanceValidOptionsWithCustomInstanceId
          ]
        }
        const res = {
          status: sinon.spy(function () { return this }),
          json: sinon.spy()
        }

        const next = sinon.spy(function (err) {
          expect(err).to.not.exist
          res.cobuildPluggables.currentConfigStep.commit(true) //if you set to false, it should throw an exception instead call to res.send.
        })

        instance.middlewares.config(req, res, next)
        next.calledOnce.should.to.be.true
        res.status.calledOnce.should.to.be.true
        res.status.calledWith(200).should.to.be.true
        res.json.calledOnce.should.to.be.true
        res.json.getCall(0).args[0].should.to.be.deep.equal([])

        instance.options.data.module_enabled.should.to.be.equal(validOptionsWithCustomInstanceId.data.module_enabled)
        instance.options.data.requiredKey.should.to.be.equal(validOptionsWithCustomInstanceId.data.requiredKey)
      })

      it('Should commit changes in the module options when are valids', function () {
        const instance = new CobuildPluggable(validOptionsWithCustomInstanceId, objectSchema)
        expect(instance instanceof CobuildPluggable).to.be.true

        const req = {
          body: [
            otherValidOptionsWithCustomInstanceId,
            otherModuleValidOptionsWithCustomInstanceId,
            otherInstanceValidOptionsWithCustomInstanceId
          ]
        }
        const res = {
          status: sinon.spy(function () { return this }),
          json: sinon.spy()
        }

        const next = sinon.spy(function (err) {
          expect(err).to.not.exist
          res.cobuildPluggables.currentConfigStep.commit(true) //if you set true, it should throw an exception instead call to res.send.
        })

        instance.middlewares.config(req, res, next)
        next.calledOnce.should.to.be.true
        res.status.calledOnce.should.to.be.true
        res.status.calledWith(200).should.to.be.true
        res.json.calledOnce.should.to.be.true
        res.json.getCall(0).args[0].should.to.be.deep.equal([
          instance.options
        ])

        instance.options.data.module_enabled.should.to.be.equal(otherValidOptionsWithCustomInstanceId.data.module_enabled)
        instance.options.data.requiredKey.should.to.be.equal(otherValidOptionsWithCustomInstanceId.data.requiredKey)
      })

      it('Should fails commiting changes when there are invalid options for it', function () {
        const instance = new CobuildPluggable(validOptionsWithCustomInstanceId, objectSchema)
        expect(instance instanceof CobuildPluggable).to.be.true

        const req = {
          body: [
            otherInvalidOptionsWithCustomInstanceId,
            otherModuleValidOptionsWithCustomInstanceId,
            otherInstanceValidOptionsWithCustomInstanceId
          ]
        }
        const res = {
          status: sinon.spy(function () { return this }),
          json: sinon.spy()
        }

        const next = sinon.spy(function (err) {
          expect(err).to.not.exist
          res.cobuildPluggables.currentConfigStep.commit(true) //if you set true, it should throw an exception instead call to res.send.
        })

        instance.middlewares.config(req, res, next)
        next.calledOnce.should.to.be.true
        res.status.calledOnce.should.to.be.true
        res.status.calledWith(422).should.to.be.true
        res.json.calledOnce.should.to.be.true
        res.json.getCall(0).args[0].should.to.be.an('array')

        instance.options.data.module_enabled.should.to.be.equal(validOptionsWithCustomInstanceId.data.module_enabled)
        instance.options.data.requiredKey.should.to.be.equal(validOptionsWithCustomInstanceId.data.requiredKey)
      })
    })
  })
})
