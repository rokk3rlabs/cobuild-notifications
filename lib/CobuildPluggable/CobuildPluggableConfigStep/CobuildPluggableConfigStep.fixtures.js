/**
 * Created by garusis on 22/06/18.
 */
const sinon = require('sinon')
const fixtures = {}

fixtures.buildRes = function () {
  return {
    status: sinon.spy(function () {
      return this
    }),
    json: sinon.spy()
  }
}

module.exports = fixtures
