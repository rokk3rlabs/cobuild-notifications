/* eslint-disable no-undef,no-unused-expressions */
/**
 * Created by garusis on 22/06/18.
 */
const _ = require('lodash')
const sinon = require('sinon')
const {CobuildPluggableConfigStep} = require('./index')
const {buildRes} = require('./CobuildPluggableConfigStep.fixtures')

describe('CobuildPluggableConfigStep', function () {
  describe('#Constructor', function () {
    it('Should create a new CobuildPluggableConfigStep instance', function () {
      const instance = new CobuildPluggableConfigStep({})
      expect(instance instanceof CobuildPluggableConfigStep).to.be.true
    })
  })

  describe('#commit', function () {
    describe('useHttp = nil', function () {
      it('Should execute the commitList and return the array of result', function () {
        const instance = new CobuildPluggableConfigStep({})
        expect(instance instanceof CobuildPluggableConfigStep).to.be.true

        const commitedItem = {}
        const commitFunction = sinon.spy(function () { return commitedItem })

        instance.commitList.push(commitFunction)
        const results = instance.commit()

        commitFunction.calledOnce.should.to.be.true
        results.should.to.be.an('array')
        results.length.should.to.be.equal(instance.commitList.length)
        results[0].should.to.be.equal(commitedItem)
      })

      it('Should throw the errors array when isn\'t empty', function () {
        const instance = new CobuildPluggableConfigStep({})
        expect(instance instanceof CobuildPluggableConfigStep).to.be.true

        const commitFunction = sinon.spy()
        const errorThrown = new Error()

        instance.commitList.push(commitFunction)
        instance.errors.push(errorThrown)

        try {
          instance.commit()
        } catch (err) {
          expect(err).to.exist
          commitFunction.notCalled.should.to.be.true
          err.should.to.be.an('array')
          err.length.should.to.be.equal(instance.errors.length)
          err[0].should.to.be.equal(errorThrown)
        }
      })
    })

    describe('useHttp = true', function () {
      it('Should execute the commitList and send the result with status 200', function () {

        const res = buildRes()
        const instance = new CobuildPluggableConfigStep(res)
        expect(instance instanceof CobuildPluggableConfigStep).to.be.true

        const commitedItem = {}
        const commitFunction = sinon.spy(function () { return commitedItem })

        instance.commitList.push(commitFunction)
        const results = instance.commit(true)

        commitFunction.calledOnce.should.to.be.true
        expect(results).to.not.exist
        res.status.calledOnce.should.to.be.true
        res.status.calledWith(200).should.to.be.true
        res.json.calledOnce.should.to.be.true
        res.json.getCall(0).args[0].should.to.be.an('array')
        res.json.getCall(0).args[0].length.should.to.be.equal(instance.commitList.length)
        res.json.getCall(0).args[0][0].should.to.be.equal(commitedItem)
      })

      it('Should throw the errors array when isn\'t empty', function () {
        const res = buildRes()
        const instance = new CobuildPluggableConfigStep(res)
        expect(instance instanceof CobuildPluggableConfigStep).to.be.true

        const commitFunction = sinon.spy()
        const errorThrown = new Error()

        instance.commitList.push(commitFunction)
        instance.errors.push(errorThrown)

        instance.commit(true)

        commitFunction.notCalled.should.to.be.true
        res.status.calledOnce.should.to.be.true
        res.status.calledWith(422).should.to.be.true
        res.json.calledOnce.should.to.be.true
        res.json.getCall(0).args[0].should.to.be.an('array')
        res.json.getCall(0).args[0].length.should.to.be.equal(instance.errors.length)
        res.json.getCall(0).args[0][0].should.to.be.equal(errorThrown)
      })
    })
  })

  describe('#describe', function () {
    it('Should clean the main lists in the instance', function () {
      const instance = new CobuildPluggableConfigStep({})
      expect(instance instanceof CobuildPluggableConfigStep).to.be.true
      instance.flush()
      expect(instance.errors).to.be.null
      expect(instance.commitList).to.be.null
    })
  })
})
