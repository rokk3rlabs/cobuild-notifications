/**
 * Created by garusis on 28/06/18.
 */

class CobuildPluggableConfigStep {
  constructor (res) {
    this.res = res
    this.errors = []
    this.commitList = []
  }

  commit (useHttp) {
    process.nextTick(() => this.flush())
    if (this.errors.length) {
      if (useHttp) return this.res.status(422).json(this.errors)
      throw this.errors
    }
    const optionList = this.commitList.map(commitItem => commitItem())
    return useHttp ? this.res.status(200).json(optionList) : optionList
  }

  flush () {
    this.errors = null
    this.commitList = null
  }
}

module.exports = {
  CobuildPluggableConfigStep
}
