/**
 * Created by garusis on 16/05/18.
 */
const _ = require('lodash')
const Joi = require('joi')
const EventEmitter = require('events')
const deepmerge = require('deepmerge')
const {basicSchema, updateSchema} = require('./main_schemas')
const {CobuildPluggableConfigStep} = require('./CobuildPluggableConfigStep')

let currentIndex = 0

class CobuildPluggable extends EventEmitter {
  /**
   *
   * @param {Object} options Settings to configure the module instance
   * @param {String} options.module_name The name of this module
   * @param {String} options.module_label The label of this module.
   * @param {String} options.module_description The description of this module
   * @param {Object} options.data Data to configure this module.
   * @param {Boolean} options.data.module_enabled The status (enable/disable) of this module
   * @param {String} [options.data.instance_id=nextIndex] The id for this module instance. If not provided a serial index will be assigned.
   * @param {JoiSchema} createSchema An object schema that describe all options and its validations in the initial setup.
   * @param {JoiSchema} [configSchema=createSchema] An object schema that describe all options and its validations in the later setup.
   * @returns {CobuildPluggable}
   * @throws {JoiError} A Joi error is thrown when the options aren't valid agains the objectSchema.
   */
  constructor (options, createSchema, configSchema = createSchema) {
    super()

    options = _.cloneDeep(options)
    if (!options.data.instance_id) {
      options.data.instance_id = ++currentIndex
    }

    this.fullSchema = basicSchema.concat(Joi.object().keys({
      data: createSchema
    }))

    this.updateSchema = updateSchema.concat(Joi.object().keys({
      data: configSchema
    }))
    this.config(options, this.fullSchema)
    this.createMiddlewares()
  }

  set options (options) {
    this._options = deepmerge(this.options, options)
    this.emit('config', this.options)
  }

  get options () {
    return this._options || {}
  }

  validateConfig (options, schema = this.updateSchema) {
    const {error, value} = Joi.validate(options, schema)
    if (error) throw error
    return value
  }

  /**
   * @param {Object} options
   * @throws {ValidationError} A Joi error is thrown when the options aren't valid agains the validationSchema.
   */
  config (options, schema = this.updateSchema) {
    this.options = this.validateConfig(options, schema)
  }

  describe () {
    return {
      data: this.options,
      schema: this.updateSchema.describe()
    }
  }

  createMiddlewares () {
    const instance = this

    this.middlewares = {}
    this.middlewares.describe = function (req, res, next) {
      const oldSend = res.send
      res.send = function (data) {
        this.send = oldSend

        data.push(instance.describe())

        return this.send(data)
      }
      next()
    }

    this.middlewares.config = function (req, res, next) {
      if (!res.cobuildPluggables) {
        res.cobuildPluggables = {}
      }

      if (!res.cobuildPluggables.currentConfigStep) {
        res.cobuildPluggables.currentConfigStep = new CobuildPluggableConfigStep(res)
      }

      const optionModules = req.body.find(optionSet => optionSet.module_name === instance.options.module_name && optionSet.data.instance_id === instance.options.data.instance_id)
      if (!optionModules) return next()
      const currentConfigStep = res.cobuildPluggables.currentConfigStep

      try {
        const validatedOptions = instance.validateConfig(optionModules)

        currentConfigStep.commitList.push(function () {
          instance.options = validatedOptions
          return instance.options
        })
      } catch (err) {
        currentConfigStep.errors.push({
          module_name: instance.options.module_name,
          instance_id: instance.options.data.instance_id,
          errors: err
        })
      }

      next()
    }
  }
}

module.exports = {
  CobuildPluggable
}
