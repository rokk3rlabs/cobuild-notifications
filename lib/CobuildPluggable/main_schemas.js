/**
 * Created by garusis on 27/06/18.
 */
const Joi = require('joi')

const basicSchema = Joi.object().keys({
  module_name: Joi.string().required().label('Module Name')
    .description('This is the module name. It should be used to identify this module agains other modules.'),
  module_label: Joi.string().required().label('Module Label')
    .description('This is the module label. It should be used in your views to separe settings between modules'),
  module_description: Joi.string().required().label('Module Description')
    .description('This is the module description. It should be used in your views to describe this module'),
  data: Joi.object().keys({
    instance_id: Joi.any().required().label('Module Instance Id'),
    module_enabled: Joi.boolean().required().label('Module Status')
      .description('This field enable or disable the use of this module')
  }).required()
})

const updateSchema = Joi.object().keys({
  module_name: Joi.string().strip(),
  module_label: Joi.string().strip(),
  module_description: Joi.string().strip(),
  data: Joi.object().required().keys({
    instance_id: Joi.any().strip(),
    module_enabled: Joi.boolean().required()
  }).required()
})

module.exports = {
  updateSchema,
  basicSchema
}
